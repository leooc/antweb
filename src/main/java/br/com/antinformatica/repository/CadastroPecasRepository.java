package br.com.antinformatica.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.antinformatica.model.Peca;

public interface CadastroPecasRepository extends MongoRepository<Peca, String> {
	  

}
