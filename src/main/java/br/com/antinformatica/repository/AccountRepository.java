package br.com.antinformatica.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.antinformatica.model.Account;

public interface AccountRepository extends MongoRepository<Account, String> {
	  
	  public Account findByUsername(String username);

}
