package br.com.antinformatica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class AntMainApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AntMainApplication.class, args);
	}

//	@Bean
//	CommandLineRunner init(final AccountRepository accountRepository) {
//
//		return new CommandLineRunner() {
//
//			@Override
//			public void run(String... arg0) throws Exception {
//				accountRepository.save(new Account("admin", "admin"));
//
//			}
//
//		};
//
//	}

}
