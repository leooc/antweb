package br.com.antinformatica.util;

import br.com.antinformatica.dto.ContatoDTO;

public class UtilGeral {
	
	public static String montaHtmlFaleConosco(ContatoDTO contatoDto) {

		String mensagem = "<p>Alguem entrou em contato pelo Fale Conosco do Site:</p><br/><br/> "
				+ "<br/>" + "<p>Nome: "
				+ validaInformacao(contatoDto.getNome())
				+ "</p>"
				+ "<p>E-mail: "
				+ validaInformacao(contatoDto.getEmail())
				+ "</p>"
				+ "<p>Telefone: "
				+ validaInformacao(contatoDto.getTelefone())
				+ "</p>"
				+ "<p>Assunto: "
				+ validaInformacao(contatoDto.getAssunto())
				+ "</p>"
				+ "<p>Mensagem: "
				+ validaInformacao(contatoDto.getMensagem())
				+ "</p>"
				+ "<br/><br/>"
				+ "<p>Entre em contato com o cliente !</p><br/>";

		return mensagem;

	}
	
	public static String validaInformacao(String informacao) {

		String retorno = "Não foi Informado!";

		if (informacao == null) {

		} else if ("".equals(informacao)) {

		} else {
			retorno = informacao;
		}

		return retorno;

	}
	
	public static StringBuffer getTemplateEmail() {

		StringBuffer template = new StringBuffer();

		template.append("			<table align=\"center\" border=\"1\" bordercolor=\"#6A9C69\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" >");
		template.append("				<tr>");
		template.append("					<td>");
		template.append("						<img src=\"http://www.conecttidigital.com.br/blog/wp-content/uploads/2012/01/cropped-cabecalho.jpg\" style=\"display: block;\" />");
		template.append("					</td>");
		template.append("				</tr>");
		template.append("				<tr>");
		template.append("					<td style=\"padding: 70px 40px 70px 40px; background-color:#DEEDEE; font-family:sans-serif;	color:#222;\">$CONTEUDO</td>");
		template.append("				</tr>");
		template.append("				<tr>");
		template.append("					<td align=\"right\" style=\"padding: 0px 0 0px 0; background: -moz-linear-gradient(bottom, #6A9C69, #A9CCAE);\">");
		template.append("						<img src=\"http://www.conecttidigital.com.br/blog/wp-content/uploads/2012/01/cropped-cabecalho.jpg\" style=\"display: block;\" />");
		template.append("					</td>");
		template.append("				</tr>");

		return template;

	}
	
	public static String replaceAcentosPorCodigosHtml(String mensagem) {

		mensagem = mensagem.replace("á", "&#225;");
		mensagem = mensagem.replace("à", "&#224;");
		mensagem = mensagem.replace("â", "&#226;");
		mensagem = mensagem.replace("ã", "&#227;");
		mensagem = mensagem.replace("Á", "&#193;");
		mensagem = mensagem.replace("À", "&#192;");
		mensagem = mensagem.replace("Â", "&#194;");
		mensagem = mensagem.replace("Ã", "&#195;");

		mensagem = mensagem.replace("é", "&#233;");
		mensagem = mensagem.replace("è", "&#232;");
		mensagem = mensagem.replace("ê", "&#234;");
		mensagem = mensagem.replace("É", "&#201;");
		mensagem = mensagem.replace("È", "&#200;");
		mensagem = mensagem.replace("Ê", "&#202;");

		mensagem = mensagem.replace("í", "&#237");
		mensagem = mensagem.replace("ì", "&#236");
		mensagem = mensagem.replace("î", "&#238");
		mensagem = mensagem.replace("Í", "&#205");
		mensagem = mensagem.replace("Ì", "&#204");
		mensagem = mensagem.replace("Î", "&#206");

		mensagem = mensagem.replace("ó", "&#243");
		mensagem = mensagem.replace("ò", "&#242");
		mensagem = mensagem.replace("ô", "&#244");
		mensagem = mensagem.replace("õ", "&#245");
		mensagem = mensagem.replace("Ó", "&#211");
		mensagem = mensagem.replace("Ò", "&#210");
		mensagem = mensagem.replace("Ô", "&#212");
		mensagem = mensagem.replace("Õ", "&#213");

		mensagem = mensagem.replace("ú", "&#250");
		mensagem = mensagem.replace("ù", "&#249");
		mensagem = mensagem.replace("û", "&#251");
		mensagem = mensagem.replace("Ú", "&#218");
		mensagem = mensagem.replace("Ù", "&#217");
		mensagem = mensagem.replace("Û", "&#219");
		;

		mensagem = mensagem.replace("ç", "&#231;");
		mensagem = mensagem.replace("Ç", "&#199;");
		mensagem = mensagem.replace("ñ", "&#241;");
		mensagem = mensagem.replace("Ñ", "&#209;");

		return mensagem;

	}
}