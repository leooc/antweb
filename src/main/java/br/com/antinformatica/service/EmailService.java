package br.com.antinformatica.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.antinformatica.dto.DestinatarioDTO;
import br.com.antinformatica.model.EmailModel;




@Service
@Transactional(readOnly = true)
public class EmailService {
	
	@Autowired
	protected EmailModel emailModel;
	
	/**
	 * Envia um email.<br> 
	 *   
	 * @param idContratante
	 * @param titulo
	 * @param corpoMensagem
	 * @param emailDestinatario
	 * @param nomeDestinatario
	 * 
	 * @throws ServiceException
	 */
	public void enviarNoFormatoHtml(Serializable idUsuario, String titulo, String corpoMensagem, List<DestinatarioDTO> destinatarios) {
		
		emailModel.enviarNoFormatoHtml(idUsuario, titulo, corpoMensagem, destinatarios);
		
	}
		
}
