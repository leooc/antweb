package br.com.antinformatica.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.antinformatica.dto.ContatoDTO;
import br.com.antinformatica.dto.DestinatarioDTO;
import br.com.antinformatica.util.UtilGeral;

@Service("orcamentoService")
@Transactional(propagation = Propagation.REQUIRED)
public class OrcamentoServiceImpl implements OrcamentoService {

	@Autowired
	private EmailService emailService;
	
	@Override
	public void enviaOrcamento(ContatoDTO contatoDto) {
		// TODO Auto-generated method stub
		List<DestinatarioDTO> destinatarios = new ArrayList<DestinatarioDTO>();
		destinatarios.add(new DestinatarioDTO("antinformationtechnology@gmail.com","Contato"));
		
		if(contatoDto.getEmail() != null){			
			
			emailService.enviarNoFormatoHtml(	null,
					"Novo Contato do boXml - Empresa " + contatoDto.getEmpresa(), 
					UtilGeral.montaHtmlFaleConosco(contatoDto), 
					destinatarios);
		} 		
	
	}
	
	
}
