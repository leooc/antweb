package br.com.antinformatica.service;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.antinformatica.dto.PecaDTO;
import br.com.antinformatica.dto.ReturnMsgDTO;
import br.com.antinformatica.model.Peca;
import br.com.antinformatica.repository.CadastroPecasRepository;

@Service
public class CadastroPecasServiceImpl implements CadastroPecasService {

	@Autowired
	CadastroPecasRepository cadastroPecasRepository;
	
	@Override
	public ReturnMsgDTO cadastroPecas(PecaDTO pecaDto) {
		
		if(pecaDto == null || pecaDto.getNome() == null || pecaDto.getFabricante() ==null ||
				pecaDto.getModelo() == null || pecaDto.getPrecoCompra() == null){
			return new ReturnMsgDTO(-500, "Ocorreu erro no cadastro da peça, formulário vazio.");
		}
		
		String nome = "";
		for (String peca : pecaDto.getNome()) {
			nome = peca;
		}		
		String fabricante = pecaDto.getFabricante();
		String modelo = pecaDto.getModelo();
		String precoCompra = pecaDto.getPrecoCompra();
		String precoVenda = pecaDto.getPrecoVenda();
		String colaborador = SecurityContextHolder.getContext()
		        .getAuthentication().getName();
		String id = new ObjectId().toString();
		
		try{
			cadastroPecasRepository.save(new Peca(id, nome, fabricante, modelo, precoCompra, precoVenda, colaborador));
			return new ReturnMsgDTO(200, "Peça cadastrada com sucesso.");
		}catch(Exception e){
			System.out.println("exception: " + e.getMessage());
			return new ReturnMsgDTO(-500, "Ocorreu erro no cadastro da peça.");
		}
	}

	@Override
	public List<PecaDTO> obterPecas() {
		List<PecaDTO> lstPecasDto = new ArrayList<PecaDTO>();
		
		List<Peca> lstPeca = cadastroPecasRepository.findAll();
		for (Peca peca : lstPeca) {
			PecaDTO pecaDTO = new PecaDTO();
			pecaDTO.setNomeStr(peca.getNome());
			pecaDTO.setFabricante(peca.getFabricante());
			pecaDTO.setModelo(peca.getModelo());
			pecaDTO.setPrecoCompra(peca.getPrecoCompra());
			pecaDTO.setPrecoVenda(peca.getPrecoVenda());
			pecaDTO.setColaborador(peca.getColaborador());
			lstPecasDto.add(pecaDTO);
		}
		
		return lstPecasDto;
	}
	

}
