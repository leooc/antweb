package br.com.antinformatica.service;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import br.com.antinformatica.dto.RegistroDTO;
import br.com.antinformatica.dto.ReturnMsgDTO;
import br.com.antinformatica.model.Account;
import br.com.antinformatica.repository.AccountRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	AccountRepository accountRepository;

	@Override
	public ReturnMsgDTO register(RegistroDTO registroDTO) {

		String username = registroDTO.getUsername();
		String password = registroDTO.getPassword();
		String confirmPassword = registroDTO.getConfirmPassword();
		String id = new ObjectId().toString();

		if (password.equals(confirmPassword) && password != null) {
			if (accountRepository.findByUsername(username) == null) {
				accountRepository.save(new Account(id, username, password));
				return new ReturnMsgDTO(200, "Usuário cadastrado com sucesso.");
			} else return new ReturnMsgDTO(-500, "Email já cadastrado no sistema");
		} else return new ReturnMsgDTO(-500, "As senhas digitadas nao coincidem.");
	}
	
	@Override
	public String getLoggedUserEmail() {
		User user;

		try {
			user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (ClassCastException ex) {
			return null;
		}

		return user.getUsername();
	}
	
	
}
