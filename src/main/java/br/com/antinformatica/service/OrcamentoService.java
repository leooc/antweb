package br.com.antinformatica.service;

import br.com.antinformatica.dto.ContatoDTO;

public interface OrcamentoService {

	public void enviaOrcamento(ContatoDTO contatoDTO);
	
}
