package br.com.antinformatica.service;

import br.com.antinformatica.dto.RegistroDTO;
import br.com.antinformatica.dto.ReturnMsgDTO;

public interface LoginService {

    public ReturnMsgDTO register(RegistroDTO registroDTO);

    String getLoggedUserEmail();

}
