package br.com.antinformatica.service;

import java.util.List;

import br.com.antinformatica.dto.PecaDTO;
import br.com.antinformatica.dto.ReturnMsgDTO;

public interface CadastroPecasService {
	
	ReturnMsgDTO cadastroPecas(PecaDTO pecaDto);

	List<PecaDTO> obterPecas();

}
