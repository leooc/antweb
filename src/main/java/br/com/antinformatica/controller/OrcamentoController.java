package br.com.antinformatica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.antinformatica.dto.ContatoDTO;
import br.com.antinformatica.service.OrcamentoService;



@Controller
public class OrcamentoController {
	
	@Autowired
	private OrcamentoService orcamentoService;
	
	@RequestMapping(value="/orcamento", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ContatoDTO recebaNovidades(@RequestBody ContatoDTO contatoDto){
		if(contatoDto != null){
			contatoDto.setValido(true);
			orcamentoService.enviaOrcamento(contatoDto);
		}
		return new ContatoDTO();		
	}
	
}
