package br.com.antinformatica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.antinformatica.dto.PecaDTO;
import br.com.antinformatica.service.CadastroPecasService;



@Controller
public class MonitorController {
	
	@Autowired
	private CadastroPecasService cadastroPecasService;
	
	@RequestMapping(value="/obterPecas", method=RequestMethod.POST,produces={"application/json"})
	public @ResponseBody List<PecaDTO> obterPecas(){
		
		return cadastroPecasService.obterPecas();
		
	}
	
	@RequestMapping(value="/obterUsuarioLogado", method=RequestMethod.POST,produces={"application/json"})
	public @ResponseBody String obterUsuarioLogado(){
		
		String usuario = SecurityContextHolder.getContext()
        .getAuthentication().getName();
		
		return usuario;
		
	}
	
}
