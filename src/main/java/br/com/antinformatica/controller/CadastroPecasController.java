package br.com.antinformatica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.antinformatica.dto.PecaDTO;
import br.com.antinformatica.service.CadastroPecasService;



@Controller
public class CadastroPecasController {
	
	@Autowired
	private CadastroPecasService cadastroPecasService;
	
	@RequestMapping(value="/cadastroPecas", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody PecaDTO cadastroPecas(@RequestBody PecaDTO pecaDTO){
		if(pecaDTO != null){
			cadastroPecasService.cadastroPecas(pecaDTO);
		}
		return new PecaDTO();		
	}
	
}
