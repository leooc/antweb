package br.com.antinformatica.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.antinformatica.dto.RegistroDTO;
import br.com.antinformatica.dto.ReturnMsgDTO;
import br.com.antinformatica.service.LoginService;

@RestController
public class LoginController {

	@Autowired
	LoginService loginService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = {"application/json"})
	public @ResponseBody ReturnMsgDTO register(@Valid @RequestBody RegistroDTO registroDTO, BindingResult bindingResults) {
		
		if (bindingResults.hasErrors()) {
            //...handle validation errors...
			System.out.println("erro");
        } else {
            //...bean is valid! Save that movie!...
        	System.out.println("ok");
        }

		return loginService.register(registroDTO);
	}
	
}
