package br.com.antinformatica.dto;

public class DestinatarioDTO {

	private String email;
	private String nome;
	private String mensagem;
	private String telefone;
	private String assunto;
	
	public DestinatarioDTO( String email, String nome ) {
		this.email = email;
		this.nome = nome;
	}
	
	
	
	public String getTelefone() {
		return telefone;
	}



	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}



	public String getAssunto() {
		return assunto;
	}



	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}



	public String getMensagem() {
		return mensagem;
	}



	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}



	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
