package br.com.antinformatica.dto;

public class ReturnMsgDTO {

    private Integer code;
    private String message;
    private String details;

    public ReturnMsgDTO() {
    }

    public ReturnMsgDTO(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ReturnMsgDTO(Integer code, String message, String details) {
        this.code = code;
        this.message = message;
        this.details = details;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }
}
