package br.com.antinformatica.dto;

public class ContatoDTO {

	private String empresa;
	
	private String cnpj;
	
	private String nome;
	
	private String contato;
	
	private String email;
	
	private String telefone;
	
	private String celular;
	
	private String assunto;
	
	private String mensagem;
	
	private String departamento;
	
	private String cargo;
	
	private String tipoContato;
	
	private boolean isFaleConosco = false;
	
	private boolean valido = false;
	
	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public ContatoDTO() {}	
	
	public void limpa() {
		
		setEmpresa("");
		setCnpj("");
		setContato("");
		setEmail("");
		setTelefone("");
		setCelular("");
		setDepartamento("");
		setCargo("");
		
				
	}	
	
	@Override
	public String toString() {
		return "ContatoDTO [empresa=" + empresa + ", cnpj=" + cnpj + ", nome=" + nome + ", contato=" + contato
				+ ", email=" + email + ", telefone=" + telefone + ", celular=" + celular + ", assunto=" + assunto
				+ ", mensagem=" + mensagem + ", departamento=" + departamento + ", cargo=" + cargo + ", tipoContato="
				+ tipoContato + ", isFaleConosco=" + isFaleConosco + ", valido=" + valido + "]";
	}

	/**
	 * @return the tipoContato
	 */
	public String getTipoContato() {
		return tipoContato;
	}

	/**
	 * @param tipoContato the tipoContato to set
	 */
	public void setTipoContato(String tipoContato) {
		this.tipoContato = tipoContato;
	}
	
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	/**
	 * @return the assunto
	 */
	public String getAssunto() {
		return assunto;
	}

	/**
	 * @param assunto the assunto to set
	 */
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	/**
	 * @return the mensagem
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * @param mensagem the mensagem to set
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * @return the isFaleConosco
	 */
	public boolean isFaleConosco() {
		return isFaleConosco;
	}

	/**
	 * @param isFaleConosco the isFaleConosco to set
	 */
	public void setFaleConosco(boolean isFaleConosco) {
		this.isFaleConosco = isFaleConosco;
	}
	
	
	
				
}

