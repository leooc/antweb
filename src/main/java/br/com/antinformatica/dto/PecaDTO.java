package br.com.antinformatica.dto;

import java.util.List;

public class PecaDTO {

	private List<String> nome;
	
	private String nomeStr;
	
	private String fabricante;
	
	private String modelo;
	
	private String precoCompra;
	
	private String precoVenda;
	
	private String colaborador;
	
	public PecaDTO() {}

	
	public String getColaborador() {
		return colaborador;
	}

	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}

	public String getNomeStr() {
		return nomeStr;
	}



	public void setNomeStr(String nomeStr) {
		this.nomeStr = nomeStr;
	}



	public List<String> getNome() {
		return nome;
	}


	public void setNome(List<String> nome) {
		this.nome = nome;
	}


	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPrecoCompra() {
		return precoCompra;
	}

	public void setPrecoCompra(String precoCompra) {
		this.precoCompra = precoCompra;
	}

	public String getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(String precoVenda) {
		this.precoVenda = precoVenda;
	}

	@Override
	public String toString() {
		return "PecaDTO [nome=" + nome + ", fabricante=" + fabricante + ", modelo=" + modelo + ", precoCompra="
				+ precoCompra + ", precoVenda=" + precoVenda + "]";
	}
				
}

