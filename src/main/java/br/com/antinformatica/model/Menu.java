package br.com.antinformatica.model;

/**
 * Created by leonardo on 10/06/16.
 */
public class Menu {

	private String id;
	private String url;
	private String name;
	private String icon;

	public Menu() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
