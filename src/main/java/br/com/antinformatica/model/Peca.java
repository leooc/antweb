package br.com.antinformatica.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Peca {
	  
	  @Id
	  private String id;
	  private String nome;
	  private String fabricante;
	  private String modelo;
	  private String precoCompra;
	  private String precoVenda;
	  private String colaborador;
	  
	  public Peca(){}
	  
	  public Peca(String id, String nome, String fabricante, String modelo, String precoCompra, String precoVenda, String colaborador) {
		  this.id = id;
		  this.nome = nome;
	      this.fabricante = fabricante;
	      this.modelo = modelo;
	      this.precoCompra = precoCompra;
	      this.precoVenda = precoVenda;
	      this.colaborador = colaborador;
	  }
	  
	  public String getId() {
	    return id;
	  }
	  public void setId(String id) {
	    this.id = id;
	  }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getColaborador() {
		return colaborador;
	}

	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPrecoCompra() {
		return precoCompra;
	}

	public void setPrecoCompra(String precoCompra) {
		this.precoCompra = precoCompra;
	}

	public String getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(String precoVenda) {
		this.precoVenda = precoVenda;
	}

}
