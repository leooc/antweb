package br.com.antinformatica.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import br.com.antinformatica.dto.DestinatarioDTO;
import br.com.antinformatica.util.UtilGeral;


@Component
public class EmailModel  {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	public EmailModel() {}
	
	public void enviarNoFormatoHtml(Serializable idUsuario, String titulo, String corpoMensagem, List<DestinatarioDTO> destinatarios) {
		
		try {			
			enviarNoFormatoHtml(titulo, aplicaTemplateEmailNaMensagem(corpoMensagem), destinatarios);
		} catch(Exception e) {
			logger.debug(e.getMessage());
		}
	}
	@Async
	private void enviarNoFormatoHtml(String titulo, String corpoMensagem, List<DestinatarioDTO> destinatarios) {		
		
		try{
            final String fromEmail = "leocostamini@gmail.com"; //requires valid gmail id
            final String password = "l1l2l3l4l5...."; // correct password for gmail id
            final String toEmail = "antinformationtechnology@gmail.com"; // can be any email id 

            System.out.println("TLSEmail Start");
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
            props.put("mail.smtp.port", "587"); //TLS Port
            props.put("mail.smtp.auth", "true"); //enable authentication
            props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

                //create Authenticator object to pass in Session.getInstance argument
            Authenticator auth = new Authenticator() {
                //override the getPasswordAuthentication method
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, password);
                }
            };
            Session session = Session.getInstance(props, auth);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            message.setContent(corpoMensagem, "text/html; charset=utf-8");

            System.out.println("Mail Check 2");

            message.setSubject("Contato de Cliente - Orçamento ou Informações ");

            System.out.println("Mail Check 3");

            Transport.send(message);
            System.out.println("Mail Sent");
        }catch(Exception ex){
            System.out.println("Mail fail");
            System.out.println(ex);
        }
    
	}	
		
	private String aplicaTemplateEmailNaMensagem(String conteudoDaMensagem) throws Exception {

		String htmlDoTemplateDoEmail = UtilGeral.getTemplateEmail().toString();
		conteudoDaMensagem = UtilGeral.replaceAcentosPorCodigosHtml(conteudoDaMensagem);
		return htmlDoTemplateDoEmail.replace("$CONTEUDO", conteudoDaMensagem);
		
	}	
	
}
