package br.com.antinformatica.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {

		web.ignoring().antMatchers("/shieldtheme/assets/**", "/shieldtheme/**", "/shieldtheme");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.formLogin().defaultSuccessUrl("/home.html").and().logout().logoutSuccessUrl("/shieldtheme/index.html")
				.permitAll().and()
				.authorizeRequests().antMatchers("/shieldtheme", "/shieldtheme/index.html",
						"/shieldtheme/registro.html", "/shieldtheme/login.html", "/login")
				.permitAll().and().csrf().disable();

		//http.authorizeRequests().antMatchers("/login").authenticated();
		http.authorizeRequests().anyRequest().authenticated();

	}

}
