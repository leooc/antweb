package br.com.antinformatica.security;

/**
@Service
public class MongoDBAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    AccessRepository accessRepository;

    @Override
    protected void additionalAuthenticationChecks
            (UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
            throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser
            (String email, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
            throws AuthenticationException {

        Access access = accessRepository.findByEmail(email);

        if (access == null) {
            throw new UsernameNotFoundException(email);
        } else {
            if (passwordMatch(usernamePasswordAuthenticationToken, access)) {
                return new User(access.getEmail(), access.getPassword(),
                        Arrays.asList(new SimpleGrantedAuthority(access.getRole())));
            } else {
                throw new UsernameNotFoundException(email);
            }
        }
    }

    private boolean passwordMatch(UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken,
                                  Access access) {
        if (PasswordUtils.encrypt(usernamePasswordAuthenticationToken.getCredentials().toString()).
                equals(access.getPassword())) return true;
        else return false;
    }
}
*/
