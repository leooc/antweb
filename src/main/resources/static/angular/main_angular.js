var app = angular.module('AntInformaticaApp', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider.when('/home', {
        templateUrl: 'home.html',
        controller: 'HomeController'
    });
    
    $routeProvider.when('/cadastroPecas', {
        templateUrl: 'cadastropecas.html',
        controller: 'CadastroPecasController'
    });

    $routeProvider.otherwise({redirectTo: '/home'});

    $locationProvider.html5Mode(true);

}]);

app.controller('HomeController', ['$scope', '$http', '$window', function ($scope, $http, $window) {

	$scope.logout = function(){
		window.location = '/logout';
	}

		
}]);